import time
import os.path
import pickle
import numpy as np
import torch
import torch.nn.functional as F
import torch.nn as nn
import torch.optim as optim
from shutil import copyfile
from PIL import Image
from envs import create_atari_env
from model import ActorCritic
from utils import pp


def ensure_shared_grads(model, shared_model):
    for param, shared_param in zip(model.parameters(), shared_model.parameters()):
        if shared_param.grad is not None:
            return
        shared_param._grad = param.grad

def save_checkpoint(args, model, results):
    pp("results['policy_loss'][-1]")
    pp("results['value_loss'][-1]")
    pp("results['prediction_loss'][-1]")
    if os.path.isfile("checkpoint/shared_model"):
        copyfile("checkpoint/shared_model", "checkpoint/shared_model_backup")
    torch.save(model, "checkpoint/shared_model")
    if os.path.isfile("checkpoint/results"):
        copyfile("checkpoint/results", "checkpoint/results_backup")
    pickle.dump(results, open("checkpoint/results", "wb"))
    print('Checkpoint saved')

def train(rank, args, shared_model, counter, lock, optimizer=None):
    test_mode = rank == 0
    torch.manual_seed(args.seed + rank)

    env = create_atari_env(args.env_name)
    env.seed(args.seed + rank)
    model = ActorCritic(env.observation_space.shape[0], env.action_space)

    prediction_criterion = nn.MSELoss()

    if test_mode: # test process
        start_time = time.time()
        reward_sum = 0
        prediction_loss_sum = 0
        policy_loss_sum = 0
        value_loss_sum = 0
        model.eval()
        if os.path.isfile("checkpoint/results"):
            results = pickle.load(open("checkpoint/results", "rb" ))
        else:
            results = {"time": [],
                       "num_steps": [],
                       "fps": [],
                       "episode_reward": [],
                       "episode_length": [],
                       "policy_loss": [],
                       "value_loss": [],
                       "prediction_loss": []
            }
    else: # training process
        if optimizer is None:
            optimizer = optim.Adam(shared_model.parameters(), lr=args.lr)
        model.train()


    state = env.reset().astype(np.float32)
    state = torch.from_numpy(state)
    done = True

    episode_length = 0
    
    while True:
        # Sync with the shared model
        model.load_state_dict(shared_model.state_dict())
        if done:
            cx = torch.zeros(1, 256)
            hx = torch.zeros(1, 256)
        else:
            cx = cx.detach()
            hx = hx.detach()

        values = []
        logits = []
        rewards = []
        states = []
        predictions = []

        for step in range(args.num_steps):
            episode_length += 1
            value, logit, prediction, (hx, cx) = model((state.unsqueeze(0), (hx, cx)))
            action = logit.detach()
            state, reward, done, _ = env.step(action.item())
            state = state.astype(np.float32)
            done = done or episode_length >= args.max_episode_length
            #reward = max(min(reward, 1), -1)

            if args.render:
                env.render()

            with lock:
                counter.value += 1

            if done:
                state = env.reset().astype(np.float32)

            state = torch.from_numpy(state)
            values.append(value)
            logits.append(logit)
            rewards.append(reward)
            states.append(state)
            predictions.append(prediction[0])
            if test_mode:
                reward_sum += reward

            if done:
                if test_mode:
                    ep_time = time.time() - start_time
                    ep_fps = counter.value / (time.time() - start_time)
                    print("Time {}m, num steps {}, FPS {:.0f}, episode reward {}, episode length {}".format(
                        int(ep_time / 60),
                        counter.value, ep_fps,
                        reward_sum, episode_length))
                    results['time'].append(ep_time)
                    results['num_steps'].append(counter.value)
                    results['fps'].append(ep_fps)
                    results['episode_reward'].append(reward_sum)
                    results['episode_length'].append(episode_length)
                    results['policy_loss'].append(policy_loss_sum / episode_length)
                    results['value_loss'].append(value_loss_sum / episode_length)
                    results['prediction_loss'].append(prediction_loss_sum / episode_length)
                    save_checkpoint(args, model, results)
                    reward_sum = 0
                    prediction_loss_sum = 0
                    policy_loss_sum = 0
                    value_loss_sum = 0
                    episode_length = 0
                    time.sleep(args.sleep_time)
                break

        R = torch.zeros(1, 1)
        if not done:
            value, _, _, _ = model((state.unsqueeze(0), (hx, cx)))
            R = value.detach()

        values.append(R)
        policy_loss = 0
        value_loss = 0
        prediction_loss = 0
        gae = torch.zeros(1, 1)
        for i in reversed(range(len(rewards))):
            R = args.gamma * R + rewards[i]
            advantage = R - values[i]
            value_loss = value_loss + 0.5 * advantage.pow(2)

            # Generalized Advantage Estimation
            delta_t = rewards[i] + args.gamma * \
                values[i + 1] - values[i]
            gae = gae * args.gamma * args.gae_lambda + delta_t
            policy_loss = policy_loss - logits[i] * gae.detach()
            prediction_loss += prediction_criterion(predictions[i], states[i])

        if test_mode:
            policy_loss_sum += policy_loss.item()
            value_loss_sum += value_loss.item()
            prediction_loss_sum += prediction_loss.item()
        else:
            optimizer.zero_grad()
            loss = policy_loss
            loss += args.value_loss_coef * value_loss
            #loss += args.prediction_loss_coef * prediction_loss
            (loss).backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), args.max_grad_norm)
            ensure_shared_grads(model, shared_model)
            optimizer.step()
