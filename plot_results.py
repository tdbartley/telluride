#!/usr/bin/env python

import argparse
import os.path
import pickle
import numpy as np
import matplotlib.pyplot as plt
from utils import pp

parser = argparse.ArgumentParser(description='agent')
parser.add_argument('--agent-dir', default="agents/hier",
                    help='directory to agent source code')
parser.add_argument('--num-samples', type=int, default=5,
                    help='number of samples for running mean')
parser.add_argument('--sleep-time', type=int, default=30,
                    help='number of seconds for test process to sleep')


vars = ('episode_reward',
        'episode_length',
        'policy_loss',
        'value_loss',
        'prediction_loss',
)

def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)

if __name__ == '__main__':
    args = parser.parse_args()

    if os.path.isfile("checkpoint/results"):
        results = pickle.load(open("checkpoint/results", "rb" ))

        for var in vars:
            fig, ax = plt.subplots()
            y = results[var]
            x = np.array(results['time']) / 60
            ax.plot(x, y)
            ax.set_xlabel('Training time (m)')
            ax.set_ylabel(var)
            plt.savefig('results/' + var + '.pdf')

        fig, ax = plt.subplots()
        y = results[var]
        ax.plot(y)
        ax.set_xlabel('Episodes')
        ax.set_ylabel(var)
        plt.savefig('results/' + var + '.pdf')
        print("Results saved to " + 'results/')

    else:
        print("Results not found!")
