#!/usr/bin/env python

from __future__ import print_function

import argparse
import os

import torch
import torch.multiprocessing as mp

import my_optim
from envs import create_atari_env
from model import ActorCritic
from train import train

parser = argparse.ArgumentParser(description='A3C')
parser.add_argument('--lr', type=float, default=0.00000000001,
                    help='learning rate')
parser.add_argument('--gamma', type=float, default=0.999,
                    help='discount factor for rewards (default: 0.99)')
parser.add_argument('--gae-lambda', type=float, default=1.00,
                    help='lambda parameter for GAE (default: 1.00)')
parser.add_argument('--entropy-coef', type=float, default=0.01,
                    help='entropy term coefficient (default: 0.01)')
parser.add_argument('--value-loss-coef', type=float, default=0.5,
                    help='value loss coefficient (default: 0.5)')
parser.add_argument('--prediction-loss-coef', type=float, default=0.01,
                    help='prediction loss coefficient')
parser.add_argument('--max-grad-norm', type=float, default=50,
                    help='value loss coefficient (default: 50)')
parser.add_argument('--seed', type=int, default=1,
                    help='random seed (default: 1)')
parser.add_argument('--num-processes', type=int, default=32,
                    help='how many training processes to use')
parser.add_argument('--num-steps', type=int, default=200,
                    help='number of forward steps in A3C')
parser.add_argument('--sleep-time', type=int, default=30,
                    help='number of seconds for test process to sleep')
parser.add_argument('--max-episode-length', type=int, default=2000,
                    help='maximum length of an episode')
parser.add_argument('--env-name', default='CartPoleMod-v0',
                    help='environment to train on')
parser.add_argument('--no-shared', default=False,
                    help='use an optimizer without shared momentum.')
parser.add_argument('--render', default=False,
                    help='render game display.')
parser.add_argument('--save-episode', default=False,
                    help='save episode states and predictions')


if __name__ == '__main__':
    os.environ['OMP_NUM_THREADS'] = '1'
    os.environ['CUDA_VISIBLE_DEVICES'] = ""

    args = parser.parse_args()

    torch.manual_seed(args.seed)
    env = create_atari_env(args.env_name)

    if os.path.isfile("checkpoint/shared_model"):
        shared_model = torch.load("checkpoint/shared_model")
        print("Checkpoint loaded")
    else:
        shared_model = ActorCritic(env.observation_space.shape[0], env.action_space)
    shared_model.share_memory()

    if args.no_shared:
        optimizer = None
    else:
        optimizer = my_optim.SharedAdam(shared_model.parameters(), lr=args.lr)
        optimizer.share_memory()

    processes = []

    counter = mp.Value('i', 0)
    lock = mp.Lock()

    #p = mp.Process(target=test, args=(args.num_processes, args, shared_model, counter))
    #p.start()
    #processes.append(p)

    for rank in range(0, args.num_processes):
        p = mp.Process(target=train, args=(rank, args, shared_model, counter, lock, optimizer))
        p.start()
        processes.append(p)
    for p in processes:
        p.join()
